class DoNothingYet < ActiveRecord::Migration[5.2]
  # rails generate migration DoNothingYet
  # rails db:migrate
  # rails db:migrate VERSION=0 - revert to version 0 migration
  # rails db:migrate:up VERSION=(Migration file name ID) - up only that migration file
  # rails db:migrate:down VERSION=(Migration file name ID) - down only that migration file
  # rails db:migrate:redo VERSION=(Migration file name ID) - redo only that migration file
  # rails db:migrate:status
  def change
  end
end
