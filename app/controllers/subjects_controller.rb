class SubjectsController < ApplicationController
  # rails generate controller Subjects index show new edit delete
  # Update routes

  layout 'admin'

  before_action :confirm_logged_in
  before_action :set_subject_count, :only => [:new, :create, :edit, :update]

  def index # GET - Show all records
    logger.debug("*** Testing the logger. ***")
    @subjects = Subject.sorted
  end

  def show # GET - Show one record
    @subject = Subject.find(params[:id])
  end

  def new # GET - Show default/new record
    @subject = Subject.new({:name => 'Default'})
  end

  def create # POST - Process new record (save to db and handle errors)
    # Instantiate a new object using form parameters
    @subject = Subject.new(subject_params)
    # Save the object
    if @subject.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Subject created successfully."
      redirect_to(subjects_path)
    else
      # If save fails, redisplay the form so user can fix problems
      render('new')
    end
  end

  def edit # GET - Show record that is going to be updated
    @subject = Subject.find(params[:id])
  end

  def update # POST - Process edited record (save changes to db and handle errors)
    # Find a new object using form parameters
    @subject = Subject.find(params[:id])
    # Update the object
    if @subject.update_attributes(subject_params)
      # If save succeeds, redirect to the index action
      flash[:notice] = "Subject updated successfully."
      redirect_to(subject_path(@subject))
    else
      # If save fails, redisplay the form so user can fix problems
      render('edit')
    end
  end

  def delete # GET - Display record about to delete
    @subject = Subject.find(params[:id])
  end

  def destroy
    @subject = Subject.find(params[:id])
    @subject.destroy
    flash[:notice] = "Subject '#{@subject.name}' destroyed successfully."
    redirect_to(subjects_path)
  end

  private

  def subject_params
    params.require(:subject).permit(:name, :position, :visible, :created_at)
  end

  def set_subject_count
    @subject_count = Subject.count
    if params[:action] == 'new' || params[:action] == 'create'
      @subject_count += 1
    end
  end

end
