class SectionEdit < ApplicationRecord
    # rails generate model SectionEdit - Also creates file in db/migrate, SingularName

    belongs_to :admin_user
    belongs_to :section

end
